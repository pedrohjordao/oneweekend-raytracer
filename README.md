# About

This is a Rust implementation of the raytracer presented in the [Ray Tracing in One Weekend](https://raytracing.github.io/books) texts. 

Both "Ray Tracing in One Weekend"  and "Ray Tracing The Next Week" are implemented, but the third volume is still left to be done. Feel free to open a PR containing the implementation.

The code is not thoroughly tested or particularly organized, this was just meant to do something fun over a couple of weeks.

I try to follow the same general implementation, but images are generated using the [Image](https://docs.rs/image) library. I also tried to move as possible in compile time. I take this opportunity to learn about the usage of the unstable const generics and functions Rust features.

# Samples
![](samples/image.jpeg)
![](samples/image2.png)

# Bugs

- Light is getting over-absorbed, resulting in darker images than the source material. This is probably related to 
  a few assumptions I've made during the first part of the text regarding the possible values of `LinearLight`.
- BVH is not working 100%.



