use crate::{dims::Dim, point::Point3, vec3::Vec3};

pub struct Ray {
    pub origin: Point3,
    pub direction: Vec3<f64>,
    pub time: f64,
}

impl Ray {
    pub fn new(origin: Point3, direction: Vec3<f64>, time: f64) -> Self {
        Ray {
            origin,
            direction,
            time,
        }
    }

    pub fn at(&self, t: f64) -> Point3 {
        let scaled_direction = self.direction * t;
        Point3::xyz(
            scaled_direction[Dim::X],
            scaled_direction[Dim::Y],
            scaled_direction[Dim::Z],
        ) + self.origin
    }
}
