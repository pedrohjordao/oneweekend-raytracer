use crate::utils::hlpr::array_from;
use num::{self, Zero};
use std::default::Default;
use std::fmt::{Debug, Error, Formatter};
use std::iter::{IntoIterator, Iterator};
use std::mem::MaybeUninit;
use std::ops::{
    Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Sub, SubAssign,
};
use std::slice;

#[derive(Clone, Copy)]
pub struct ConstArray<T, const D: usize> {
    values: [T; D],
}

pub struct IntoIter<T, const D: usize> {
    counter: usize,
    values: ConstArray<T, D>,
}

pub struct IterMut<'a, T: 'a> {
    iter: slice::IterMut<'a, T>,
}

pub struct Iter<'a, T: 'a> {
    iter: slice::Iter<'a, T>,
}

impl<'a, T> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl<T, const D: usize> ConstArray<T, D> {
    pub fn len(&self) -> usize {
        self.values.len()
    }

    pub fn generating<F: Fn(usize) -> T>(generator: F) -> ConstArray<T, D> {
        ConstArray {
            values: array_from(generator),
        }
    }

    pub fn iter(&self) -> Iter<T> {
        Iter {
            iter: self.values.iter(),
        }
    }

    pub fn iter_mut(&mut self) -> IterMut<T> {
        IterMut {
            iter: self.values.iter_mut(),
        }
    }
}

impl<T: Clone, const D: usize> Iterator for IntoIter<T, D> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.counter += 1;
        if self.counter <= self.values.len() {
            Some(self.values[self.counter - 1].clone())
        } else {
            None
        }
    }
}

impl<T: Clone, const D: usize> IntoIterator for ConstArray<T, D> {
    type Item = T;
    type IntoIter = IntoIter<T, D>;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter {
            counter: 0,
            values: self,
        }
    }
}

impl<'a, T, const D: usize> IntoIterator for &'a ConstArray<T, D> {
    type Item = &'a T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, T, const D: usize> IntoIterator for &'a mut ConstArray<T, D> {
    type Item = &'a mut T;
    type IntoIter = IterMut<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<T: PartialEq<E>, E, const D: usize> PartialEq<ConstArray<E, { D }>> for ConstArray<T, { D }> {
    fn eq(&self, other: &ConstArray<E, { D }>) -> bool {
        self.values
            .iter()
            .zip(other.values.iter())
            .all(|(x, y)| x.eq(y))
    }
}

impl<T, const D: usize> Index<usize> for ConstArray<T, { D }> {
    type Output = T;

    fn index(&self, idx: usize) -> &Self::Output {
        &self.values[idx]
    }
}

impl<T, const D: usize> IndexMut<usize> for ConstArray<T, { D }> {
    fn index_mut(&mut self, idx: usize) -> &mut Self::Output {
        &mut self.values[idx]
    }
}

impl<T, const D: usize> Debug for ConstArray<T, { D }>
where
    T: Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        write!(f, "[")?;
        for i in 0..D {
            write!(f, "{:?}", i)?;
        }
        write!(f, "[")?;
        Ok(())
    }
}

impl<T, const D: usize> From<[T; D]> for ConstArray<T, { D }> {
    #[inline]
    fn from(src: [T; D]) -> Self {
        assert!(D > 0);
        Self { values: src }
    }
}

impl<T: Clone, const D: usize> From<&[T]> for ConstArray<T, { D }> {
    #[inline]
    fn from(src: &[T]) -> Self {
        assert!(src.len() <= D, "Array is not long enough");
        Self::from(array_from(|i| unsafe { src.get_unchecked(i).clone() }))
    }
}

impl<T: Default, const D: usize> Default for ConstArray<T, { D }> {
    fn default() -> Self {
        ConstArray {
            values: array_from(|_| T::default()),
        }
    }
}

impl<T: Default + Clone, const D: usize> ConstArray<T, { D }> {
    fn casting<E: Default>(self) -> ConstArray<E, { D }>
    where
        E: From<T>,
    {
        let mut values: MaybeUninit<[E; { D }]> = MaybeUninit::uninit();
        let iter = self.into_iter().map(|x| x.into());

        for (cnt, val) in iter.enumerate() {
            let ptr = values.as_mut_ptr() as *mut E;
            unsafe { ptr.add(cnt).write(val) }
        }

        let values = unsafe { values.assume_init() };

        ConstArray { values }
    }

    fn new_size<const D2: usize>(&self) -> ConstArray<T, { D2 }> {
        let mut res = ConstArray::<T, { D2 }>::default();
        res.values
            .iter_mut()
            .zip(self.values.iter())
            .for_each(|(a, b)| *a = b.clone());
        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn resizes_as_expected() {
        let res = ConstArray { values: [1, 2, 3] };
        let _: ConstArray<_, 2> = res.new_size();
        assert_eq!(
            res.new_size(),
            ConstArray { values: [1, 2] },
            "Dropped wrong elements when shrinking"
        );
        assert_eq!(
            res.new_size(),
            ConstArray {
                values: [1, 2, 3, 0]
            },
            "Added elements in the wrong place when growing"
        );
        assert_eq!(
            res,
            ConstArray { values: [1, 2, 3] },
            "Original data was modified"
        );
    }
}
