use crate::const_array::{self, ConstArray};
use crate::dims::Dim;
use crate::{point::Point3, utils::hlpr::array_from};
use num::{self, One, Zero};
use rand::prelude::*;
use std::fmt::Debug;
use std::iter::{IntoIterator, Iterator};
use std::mem::MaybeUninit;
use std::ops::{
    Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Sub, SubAssign,
};

pub trait Num: num::Num + Copy + Clone {}

impl<T> Num for T where T: num::Num + Copy + Clone + std::fmt::Debug {}

#[derive(Clone, Copy, Debug)]
pub struct Vec3<T>(ConstArray<T, 3>);

impl<T> Vec3<T> {
    pub fn iter(&self) -> Iter<T> {
        Iter {
            iter: self.0.iter(),
        }
    }
    pub fn iter_mut(&mut self) -> IterMut<T> {
        IterMut {
            iter: self.0.iter_mut(),
        }
    }
}

impl<T: num::Float + Debug> Vec3<T> {
    pub fn length(&self) -> T {
        self.length_squared().sqrt()
    }
    pub fn length_squared(&self) -> T {
        self[Dim::X] * self[Dim::X] + self[Dim::Y] * self[Dim::Y] + self[Dim::Z] * self[Dim::Z]
    }

    pub fn unit(&self) -> Vec3<T> {
        let length = self.length();
        Vec3::xyz(
            self[Dim::X] / length,
            self[Dim::Y] / length,
            self[Dim::Z] / length,
        )
    }
}

impl Vec3<f64> {
    pub fn random(min: f64, max: f64, source: &mut ThreadRng) -> Self {
        let x = source.gen_range(min, max);
        let y = source.gen_range(min, max);
        let z = source.gen_range(min, max);

        Self::xyz(x, y, z)
    }

    pub fn rand_in_hemisphere(normal: &Vec3<f64>, source: &mut ThreadRng) -> Self {
        let in_unit_sphere = Self::rand_unit_sphere(source);

        if in_unit_sphere.dot(normal) > 0.0 {
            in_unit_sphere
        } else {
            -in_unit_sphere
        }
    }

    pub fn rand_lambertian(source: &mut ThreadRng) -> Self {
        use num::traits::FloatConst;
        let a = source.gen_range::<f64, f64, f64>(0.0, 2.0 * f64::PI());
        let z = source.gen_range::<f64, f64, f64>(-1.0, 1.0);
        let r = (1.0 - z * z).sqrt();
        Vec3::xyz(r * a.cos(), r * a.sin(), z)
    }

    pub fn rand_unit_sphere(source: &mut ThreadRng) -> Self {
        let limit1 = source.gen_range::<f64, f64, f64>(-1.0, 1.0);
        let squared_limit1 = limit1 * limit1;
        let sub_circle_limit = -(squared_limit1 - 1.0);
        let limit2 = source.gen_range::<f64, f64, f64>(-sub_circle_limit, sub_circle_limit);
        let squared_limit2 = limit2 * limit2;
        let sub_line_limit = -(squared_limit2 - sub_circle_limit);
        let limit3 = source.gen_range::<f64, f64, f64>(-sub_line_limit, sub_line_limit);
        Vec3::xyz(limit1, limit2, limit3)
    }

    pub fn reflect(&self, surface_normal: &Self) -> Self {
        *self - *surface_normal * self.dot(surface_normal) * 2.0
    }

    pub fn refract(&self, normal: &Vec3<f64>, etai_over_etan: f64) -> Self {
        let cos_theta = (-*self).dot(normal);
        let r_out_parllel = (*self + *normal * cos_theta) * etai_over_etan;
        let r_out_prep = *normal * -(1.0 - r_out_parllel.length_squared()).sqrt();
        r_out_prep + r_out_parllel
    }
}

impl<T: Num> Vec3<T> {
    pub fn xyz(x: T, y: T, z: T) -> Self {
        Self(ConstArray::from([x, y, z]))
    }

    pub fn x() -> Self {
        Vec3::xyz(T::one(), T::zero(), T::zero())
    }
    pub fn y() -> Self {
        Vec3::xyz(T::zero(), T::one(), T::zero())
    }
    pub fn z() -> Self {
        Vec3::xyz(T::zero(), T::zero(), T::one())
    }

    pub fn dot(&self, other: &Vec3<T>) -> T {
        self[Dim::X] * other[Dim::X] + self[Dim::Y] * other[Dim::Y] + self[Dim::Z] * other[Dim::Z]
    }

    pub fn cross(&self, other: Vec3<T>) -> Vec3<T> {
        Self(ConstArray::from([
            self[Dim::Y] * other[Dim::Z] - self[Dim::Z] * other[Dim::Y],
            self[Dim::Z] * other[Dim::X] - self[Dim::X] * other[Dim::Z],
            self[Dim::X] * other[Dim::Y] - self[Dim::Y] * other[Dim::X],
        ]))
    }
}

impl<T> From<[T; 3]> for Vec3<T> {
    #[inline]
    fn from(src: [T; 3]) -> Self {
        Self(ConstArray::from(src))
    }
}

impl<T: Clone> From<&[T]> for Vec3<T> {
    #[inline]
    fn from(src: &[T]) -> Self {
        Self(ConstArray::from(src))
    }
}

impl From<Point3> for Vec3<f64> {
    #[inline]
    fn from(src: Point3) -> Self {
        src.as_vec3()
    }
}

impl<T: Default> Default for Vec3<T> {
    #[inline]
    fn default() -> Self {
        Self(ConstArray::default())
    }
}

impl<T: Num> Zero for Vec3<T> {
    fn zero() -> Self {
        Self::from(array_from(|_| T::zero()))
    }

    fn is_zero(&self) -> bool {
        self.0.into_iter().all(|x| x == T::zero())
    }
}

impl<T: Num> One for Vec3<T> {
    fn one() -> Self {
        Self::from(array_from(|_| T::one()))
    }
}

impl<T: Num + Neg> Neg for Vec3<T>
where
    <T as Neg>::Output: Num,
{
    type Output = Vec3<<T as Neg>::Output>;

    fn neg(self) -> Self::Output {
        Vec3::xyz(-self[Dim::X], -self[Dim::Y], -self[Dim::Z])
    }
}

pub struct Iter<'a, T: 'a> {
    iter: const_array::Iter<'a, T>,
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
pub struct IterMut<'a, T: 'a> {
    iter: const_array::IterMut<'a, T>,
}
impl<'a, T> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
pub struct IntoIter<T> {
    counter: usize,
    inner: Vec3<T>,
}

impl<T: Clone> Iterator for IntoIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.counter += 1;
        if self.counter <= 3 {
            Some(self.inner.0[self.counter - 1].clone())
        } else {
            None
        }
    }
}

impl<T: Clone> IntoIterator for Vec3<T> {
    type Item = T;
    type IntoIter = IntoIter<T>;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter {
            counter: 0,
            inner: self,
        }
    }
}

impl<'a, T> IntoIterator for &'a Vec3<T> {
    type Item = &'a T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, T> IntoIterator for &'a mut Vec3<T> {
    type Item = &'a mut T;
    type IntoIter = IterMut<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<T> Index<Dim> for Vec3<T> {
    type Output = T;

    fn index(&self, idx: Dim) -> &Self::Output {
        match idx {
            Dim::X => &self.0[0],
            Dim::Y => &self.0[1],
            Dim::Z => &self.0[2],
        }
    }
}

impl<T> IndexMut<Dim> for Vec3<T> {
    fn index_mut(&mut self, idx: Dim) -> &mut Self::Output {
        match idx {
            Dim::X => &mut self.0[0],
            Dim::Y => &mut self.0[1],
            Dim::Z => &mut self.0[2],
        }
    }
}

impl<T: Num + Mul<E>, E: Num> Mul<Vec3<E>> for Vec3<T>
where
    <T as Mul<E>>::Output: Num,
{
    type Output = Vec3<<T as Mul<E>>::Output>;

    fn mul(self, rhs: Vec3<E>) -> Self::Output {
        let mut values: MaybeUninit<[<T as Mul<E>>::Output; 3]> = MaybeUninit::uninit();
        let iter = self
            .0
            .into_iter()
            .zip(rhs.0.into_iter())
            .map(|(x, y)| x * y);

        for (cnt, val) in iter.enumerate() {
            let ptr = values.as_mut_ptr() as *mut <T as Mul<E>>::Output;
            unsafe { ptr.add(cnt).write(val) }
        }

        let values = unsafe { values.assume_init() };
        Self::Output::from(values)
    }
}

impl<T: Num + Add<E>, E: Num> Add<Vec3<E>> for Vec3<T>
where
    <T as Add<E>>::Output: Num,
{
    type Output = Vec3<<T as Add<E>>::Output>;

    fn add(self, rhs: Vec3<E>) -> Self::Output {
        let mut values: MaybeUninit<[<T as Add<E>>::Output; 3]> = MaybeUninit::uninit();
        let iter = self
            .0
            .into_iter()
            .zip(rhs.0.into_iter())
            .map(|(x, y)| x + y);

        for (cnt, val) in iter.enumerate() {
            let ptr = values.as_mut_ptr() as *mut <T as Add<E>>::Output;
            unsafe { ptr.add(cnt).write(val) }
        }

        let values = unsafe { values.assume_init() };
        Self::Output::from(values)
    }
}

impl<T: Num + Add<E>, E: Num> Add<E> for Vec3<T>
where
    <T as Add<E>>::Output: Num,
{
    type Output = Vec3<<T as Add<E>>::Output>;

    fn add(self, rhs: E) -> Self::Output {
        let mut values: MaybeUninit<[<T as Add<E>>::Output; 3]> = MaybeUninit::uninit();
        let iter = self.0.into_iter().map(|x| x + rhs);

        for (cnt, val) in iter.enumerate() {
            let ptr = values.as_mut_ptr() as *mut <T as Add<E>>::Output;
            unsafe { ptr.add(cnt).write(val) }
        }

        let values = unsafe { values.assume_init() };
        Self::Output::from(values)
    }
}

impl<T: Num + Sub<E>, E: Num> Sub<E> for Vec3<T>
where
    <T as Sub<E>>::Output: Num,
{
    type Output = Vec3<<T as Sub<E>>::Output>;

    fn sub(self, rhs: E) -> Self::Output {
        let mut values: MaybeUninit<[<T as Sub<E>>::Output; 3]> = MaybeUninit::uninit();
        let iter = self.0.into_iter().map(|x| x - rhs);

        for (cnt, val) in iter.enumerate() {
            let ptr = values.as_mut_ptr() as *mut <T as Sub<E>>::Output;
            unsafe { ptr.add(cnt).write(val) }
        }

        let values = unsafe { values.assume_init() };
        Self::Output::from(values)
    }
}
impl<T: Num + Mul<E>, E: Num> Mul<E> for Vec3<T>
where
    <T as Mul<E>>::Output: Num,
{
    type Output = Vec3<<T as Mul<E>>::Output>;

    fn mul(self, rhs: E) -> Self::Output {
        let mut values: MaybeUninit<[<T as Mul<E>>::Output; 3]> = MaybeUninit::uninit();
        let iter = self.0.into_iter().map(|x| x * rhs);

        for (cnt, val) in iter.enumerate() {
            let ptr = values.as_mut_ptr() as *mut <T as Mul<E>>::Output;
            unsafe { ptr.add(cnt).write(val) }
        }

        let values = unsafe { values.assume_init() };
        Self::Output::from(values)
    }
}
impl<T: Num + Div<E>, E: Num> Div<E> for Vec3<T>
where
    <T as Div<E>>::Output: Num,
{
    type Output = Vec3<<T as Div<E>>::Output>;

    fn div(self, rhs: E) -> Self::Output {
        let mut values: MaybeUninit<[<T as Div<E>>::Output; 3]> = MaybeUninit::uninit();
        let iter = self.0.into_iter().map(|x| x / rhs);

        for (cnt, val) in iter.enumerate() {
            let ptr = values.as_mut_ptr() as *mut <T as Div<E>>::Output;
            unsafe { ptr.add(cnt).write(val) }
        }

        let values = unsafe { values.assume_init() };
        Self::Output::from(values)
    }
}

impl<T: Num + Sub<E>, E: Num> Sub<Vec3<E>> for Vec3<T>
where
    <T as Sub<E>>::Output: Num,
{
    type Output = Vec3<<T as Sub<E>>::Output>;

    fn sub(self, rhs: Vec3<E>) -> Self::Output {
        let mut values: MaybeUninit<[<T as Sub<E>>::Output; 3]> = MaybeUninit::uninit();
        let iter = self
            .0
            .into_iter()
            .zip(rhs.0.into_iter())
            .map(|(x, y)| x - y);

        for (cnt, val) in iter.enumerate() {
            let ptr = values.as_mut_ptr() as *mut <T as Sub<E>>::Output;
            unsafe { ptr.add(cnt).write(val) }
        }

        let values = unsafe { values.assume_init() };
        Self::Output::from(values)
    }
}

impl<T: Num + Div<E>, E: Num> Div<Vec3<E>> for Vec3<T>
where
    <T as Div<E>>::Output: Num,
{
    type Output = Vec3<<T as Div<E>>::Output>;

    fn div(self, rhs: Vec3<E>) -> Self::Output {
        let mut values: MaybeUninit<[<T as Div<E>>::Output; 3]> = MaybeUninit::uninit();
        let iter = self
            .0
            .into_iter()
            .zip(rhs.0.into_iter())
            .map(|(x, y)| x / y);

        for (cnt, val) in iter.enumerate() {
            let ptr = values.as_mut_ptr() as *mut <T as Div<E>>::Output;
            unsafe { ptr.add(cnt).write(val) }
        }

        let values = unsafe { values.assume_init() };
        Self::Output::from(values)
    }
}

impl<T: Num + AddAssign<E>, E: Num> AddAssign<Vec3<E>> for Vec3<T> {
    fn add_assign(&mut self, rhs: Vec3<E>) {
        self.0
            .iter_mut()
            .zip(rhs.0.into_iter())
            .for_each(|(s, b)| s.add_assign(b));
    }
}

impl<T: Num + AddAssign<E>, E: Num> AddAssign<E> for Vec3<T> {
    fn add_assign(&mut self, rhs: E) {
        self.0.iter_mut().for_each(|s| s.add_assign(rhs));
    }
}

impl<T: Num + SubAssign<E>, E: Num> SubAssign<E> for Vec3<T> {
    fn sub_assign(&mut self, rhs: E) {
        self.0.iter_mut().for_each(|s| s.sub_assign(rhs));
    }
}
impl<T: Num + MulAssign<E>, E: Num> MulAssign<E> for Vec3<T> {
    fn mul_assign(&mut self, rhs: E) {
        self.0.iter_mut().for_each(|s| s.mul_assign(rhs));
    }
}
impl<T: Num + DivAssign<E>, E: Num> DivAssign<E> for Vec3<T> {
    fn div_assign(&mut self, rhs: E) {
        self.0.iter_mut().for_each(|s| s.div_assign(rhs));
    }
}
impl<T: Num + SubAssign<E>, E: Num> SubAssign<Vec3<E>> for Vec3<T> {
    fn sub_assign(&mut self, rhs: Vec3<E>) {
        self.0
            .iter_mut()
            .zip(rhs.0.into_iter())
            .for_each(|(s, b)| s.sub_assign(b));
    }
}

impl<T: Num + MulAssign<E>, E: Num> MulAssign<Vec3<E>> for Vec3<T> {
    fn mul_assign(&mut self, rhs: Vec3<E>) {
        self.0
            .iter_mut()
            .zip(rhs.0.into_iter())
            .for_each(|(s, b)| s.mul_assign(b));
    }
}

impl<T: Num + DivAssign<E>, E: Num> DivAssign<Vec3<E>> for Vec3<T> {
    fn div_assign(&mut self, rhs: Vec3<E>) {
        self.0
            .iter_mut()
            .zip(rhs.0.into_iter())
            .for_each(|(s, b)| s.div_assign(b));
    }
}
