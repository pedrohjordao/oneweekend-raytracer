use crate::{hit::{HitRecord,  Hittable}, materials::Material, aabb::AABB, point::Point3, rays::Ray, dims::Dim, vec3::Vec3};
use std::rc::Rc;

pub struct XYRect {
    x0: f64,
    x1: f64,
    y0: f64,
    y1: f64,
    k: f64,
    mat: Rc<dyn Material>,
}

impl XYRect {
    pub fn new(mat: Rc<dyn Material>, x0: f64, x1: f64, y0: f64, y1: f64, k: f64) -> Self {
        Self {
            x0,
            x1,
            y0,
            y1,
            k,
            mat,
        }
    }
}

impl Hittable for XYRect {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let t = (self.k - r.origin[Dim::Z]) / r.direction[Dim::Z];
        if t < t_min || t > t_max {
            return None;
        }

        let x = r.origin[Dim::X] + t * r.direction[Dim::X];
        let y = r.origin[Dim::Y] + t * r.direction[Dim::Y];

        if x < self.x0 || x > self.x1 || y < self.y0 || y > self.y1 {
            None
        } else {
            let hr = HitRecord::new(
                r.at(t), 
                t, 
                self.mat.clone(), 
                Vec3::xyz(0.0, 0.0, 1.0),
                r,
                (x - self.x0) / (self.x1 - self.x0),
                (y - self.y0) / (self.y1 - self.y1)
            );
            Some(hr)
        }
    }

    fn bounding_box(&self, _t0: f64, _t1: f64) -> Option<AABB> {
        Some(
            AABB::new(Point3::xyz(self.x0, self.y0, self.k - 0.0001), Point3::xyz(self.x1, self.y1, self.k + 0.0001))
        )
    }
}

pub struct YZRect {
    y0: f64,
    y1: f64,
    z0: f64,
    z1: f64,
    k: f64,
    mat: Rc<dyn Material>,
}

impl YZRect {
    pub fn new(mat: Rc<dyn Material>, y0: f64, y1: f64, z0: f64, z1: f64, k: f64) -> Self {
        Self {
            z0,
            z1,
            y0,
            y1,
            k,
            mat,
        }
    }
}

impl Hittable for YZRect {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let t = (self.k - r.origin[Dim::X]) / r.direction[Dim::X];
        if t < t_min || t > t_max {
            return None;
        }

        let z = r.origin[Dim::Z] + t * r.direction[Dim::Z];
        let y = r.origin[Dim::Y] + t * r.direction[Dim::Y];

        if z < self.z0 || z > self.z1 || y < self.y0 || y > self.y1 {
            None
        } else {
            let hr = HitRecord::new(
                r.at(t), 
                t, 
                self.mat.clone(), 
                Vec3::xyz(1.0, 0.0, 0.0),
                r,
                (y - self.y0) / (self.y1 - self.y1),
                (z - self.z0) / (self.z1 - self.z0),
            );
            Some(hr)
        }
    }

    fn bounding_box(&self, _t0: f64, _t1: f64) -> Option<AABB> {
        Some(
            AABB::new(Point3::xyz(self.k - 0.0001, self.y0, self.z0), Point3::xyz(self.k + 0.0001, self.y1, self.z1))
        )
    }
}
pub struct XZRect {
    x0: f64,
    x1: f64,
    z0: f64,
    z1: f64,
    k: f64,
    mat: Rc<dyn Material>,
}

impl XZRect {
    pub fn new(mat: Rc<dyn Material>, x0: f64, x1: f64, z0: f64, z1: f64, k: f64) -> Self {
        Self {
            z0,
            z1,
            x0,
            x1,
            k,
            mat,
        }
    }
}

impl Hittable for XZRect {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let t = (self.k - r.origin[Dim::Y]) / r.direction[Dim::Y];
        if t < t_min || t > t_max {
            return None;
        }

        let z = r.origin[Dim::Z] + t * r.direction[Dim::Z];
        let x = r.origin[Dim::X] + t * r.direction[Dim::X];

        if z < self.z0 || z > self.z1 || x < self.x0 || x > self.x1 {
            None
        } else {
            let hr = HitRecord::new(
                r.at(t), 
                t, 
                self.mat.clone(), 
                Vec3::xyz(0.0, 1.0, 0.0),
                r,
                (x - self.x0) / (self.x1 - self.x1),
                (z - self.z0) / (self.z1 - self.z0),
            );
            Some(hr)
        }
    }

    fn bounding_box(&self, _t0: f64, _t1: f64) -> Option<AABB> {
        Some(
            AABB::new(Point3::xyz(self.x0, self.k - 0.0001, self.z0), Point3::xyz(self.x1, self.k + 0.0001, self.z1))
        )
    }
}
