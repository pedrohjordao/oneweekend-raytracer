use crate::hit::*;
use crate::point::Point3;
use crate::{aabb::AABB, dims::Dim, materials::Material, rays::Ray};
use std::f64::consts::PI;
use std::rc::Rc;

pub struct Sphere {
    center: Point3,
    radius: f64,
    material: Rc<dyn Material>,
}

impl Sphere {
    pub fn new(material: Rc<dyn Material>, center: Point3, radius: f64) -> Self {
        Sphere {
            center,
            radius,
            material,
        }
    }
}

impl Hittable for Sphere {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let oc = (r.origin - self.center).as_vec3();
        let a = r.direction.length_squared();
        let half_b = oc.dot(&r.direction);
        let c = oc.length_squared() - self.radius * self.radius;
        let discriminant = half_b * half_b - a * c;
        if discriminant > 0.0 {
            let root = discriminant.sqrt();
            let root1 = (-half_b - root) / a;
            let root2 = (-half_b - root) / a;
            if root1 < t_max && root1 > t_min {
                let at = r.at(root1);
                let point_for_uv = (at - self.center) / self.radius;
                let (u, v) = get_sphere_uv(point_for_uv);
                Some(HitRecord::new(
                    at,
                    root1,
                    self.material.clone(),
                    ((at - self.center) / self.radius).as_vec3(),
                    r,
                    u,
                    v,
                ))
            } else if root2 < t_max && root2 > t_min {
                let at = r.at(root2);
                let point_for_uv = (at - self.center) / self.radius;
                let (u, v) = get_sphere_uv(point_for_uv);
                Some(HitRecord::new(
                    at,
                    root2,
                    self.material.clone(),
                    ((at - self.center) / self.radius).as_vec3(),
                    r,
                    u,
                    v,
                ))
            } else {
                None
            }
        } else {
            None
        }
    }
    fn bounding_box(&self, _: f64, _: f64) -> Option<AABB> {
        Some(AABB::new(
            self.center - Point3::xyz(self.radius, self.radius, self.radius),
            self.center + Point3::xyz(self.radius, self.radius, self.radius),
        ))
    }
}

pub struct MovingSphere {
    center0: Point3,
    center1: Point3,
    time0: f64,
    time1: f64,
    radius: f64,
    material: Rc<dyn Material>,
}

impl MovingSphere {
    pub fn new(
        material: Rc<dyn Material>,
        center0: Point3,
        center1: Point3,
        radius: f64,
        time0: f64,
        time1: f64,
    ) -> Self {
        Self {
            center0,
            center1,
            time0,
            time1,
            radius,
            material,
        }
    }

    fn center(&self, time: f64) -> Point3 {
        self.center0
            + (self.center1 - self.center0) * ((time - self.time0) / (self.time1 - self.time0))
    }
}

impl Hittable for MovingSphere {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let oc = (r.origin - self.center(r.time)).as_vec3();
        let a = r.direction.length_squared();
        let half_b = oc.dot(&r.direction);
        let c = oc.length_squared() - self.radius * self.radius;
        let discriminant = half_b * half_b - a * c;
        if discriminant > 0.0 {
            let root = discriminant.sqrt();
            let root1 = (-half_b - root) / a;
            let root2 = (-half_b - root) / a;
            if root1 < t_max && root1 > t_min {
                let at = r.at(root1);
                let point_for_uv = (at - self.center(r.time)) / self.radius;
                let (u, v) = get_sphere_uv(point_for_uv);
                Some(HitRecord::new(
                    at,
                    root1,
                    self.material.clone(),
                    ((at - self.center(r.time)) / self.radius).as_vec3(),
                    r,
                    u,
                    v,
                ))
            } else if root2 < t_max && root2 > t_min {
                let at = r.at(root2);
                let point_for_uv = (at - self.center(r.time)) / self.radius;
                let (u, v) = get_sphere_uv(point_for_uv);
                Some(HitRecord::new(
                    at,
                    root2,
                    self.material.clone(),
                    ((at - self.center(r.time)) / self.radius).as_vec3(),
                    r,
                    u,
                    v,
                ))
            } else {
                None
            }
        } else {
            None
        }
    }
    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        let box0 = AABB::new(
            self.center(t0) - Point3::xyz(self.radius, self.radius, self.radius),
            self.center(t0) + Point3::xyz(self.radius, self.radius, self.radius),
        );
        let box1 = AABB::new(
            self.center(t1) - Point3::xyz(self.radius, self.radius, self.radius),
            self.center(t1) + Point3::xyz(self.radius, self.radius, self.radius),
        );

        Some(AABB::surrounding_box(box0, box1))
    }
}

fn get_sphere_uv(point: Point3) -> (f64, f64) {
    let phi = point[Dim::Z].atan2(point[Dim::X]);
    let theta = point[Dim::Y].asin();
    (1.0 - (phi + PI) / (2.0 * PI), (theta + PI / 2.0) / PI)
}
