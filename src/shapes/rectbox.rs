use super::rectangle::{XYRect, XZRect, YZRect};
use crate::{
    aabb::AABB,
    dims::Dim,
    hit::{FlipFace, HitRecord, Hittable},
    materials::Material,
    point::Point3,
    rays::Ray,
};
use std::rc::Rc;

pub struct RectBox {
    min: Point3,
    max: Point3,
    sides: Vec<Box<dyn Hittable>>,
}

impl RectBox {
    pub fn new(material: Rc<dyn Material>, p0: Point3, p1: Point3) -> Self {
        let sides: Vec<Box<dyn Hittable>> = vec![
            Box::new(XYRect::new(
                material.clone(),
                p0[Dim::X],
                p1[Dim::X],
                p0[Dim::Y],
                p1[Dim::Y],
                p1[Dim::Z],
            )),
            Box::new(FlipFace::new(Rc::new(XYRect::new(
                material.clone(),
                p0[Dim::X],
                p1[Dim::X],
                p0[Dim::Y],
                p1[Dim::Y],
                p0[Dim::Z],
            )))),
            Box::new(XZRect::new(
                material.clone(),
                p0[Dim::X],
                p1[Dim::X],
                p0[Dim::Z],
                p1[Dim::Z],
                p1[Dim::Y],
            )),
            Box::new(FlipFace::new(Rc::new(XZRect::new(
                material.clone(),
                p0[Dim::X],
                p1[Dim::X],
                p0[Dim::Z],
                p1[Dim::Z],
                p0[Dim::Y],
            )))),
            Box::new(YZRect::new(
                material.clone(),
                p0[Dim::Y],
                p1[Dim::Y],
                p0[Dim::Z],
                p1[Dim::Z],
                p0[Dim::X],
            )),
            Box::new(FlipFace::new(Rc::new(YZRect::new(
                material.clone(),
                p0[Dim::Y],
                p1[Dim::Y],
                p0[Dim::Z],
                p1[Dim::Z],
                p0[Dim::X],
            )))),
        ];

        Self {
            sides,
            min: p0,
            max: p1,
        }
    }
}

impl Hittable for RectBox {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        self.sides.hit(r, t_min, t_max)
    }
    fn bounding_box(&self, _t0: f64, _t1: f64) -> Option<AABB> {
        Some(AABB::new(self.min, self.max))
    }
}
