use crate::{dims::Dim, perlin::Perlin, point::Point3, rgb::LinearColor};
use image::RgbImage;

pub trait Texture {
    fn value(&self, u: f64, v: f64, p: &Point3) -> LinearColor;
}

impl Texture for LinearColor {
    fn value(&self, _: f64, _: f64, _: &Point3) -> LinearColor {
        *self
    }
}

pub struct Checkered {
    odd: Box<dyn Texture>,
    even: Box<dyn Texture>,
}

impl Checkered {
    pub fn new(odd: Box<dyn Texture>, even: Box<dyn Texture>) -> Self {
        Self { odd, even }
    }
}

impl Texture for Checkered {
    fn value(&self, u: f64, v: f64, p: &Point3) -> LinearColor {
        let sines = (10.0 * p[Dim::X]).sin() * (10.0 * p[Dim::Y]).sin() * (10.0 * p[Dim::Z]).sin();
        if sines < 0.0 {
            self.odd.value(u, v, p)
        } else {
            self.even.value(u, v, p)
        }
    }
}

pub struct NoiseTexture {
    perlin: Perlin,
    scale: f64,
}

impl NoiseTexture {
    pub fn new(scale: f64, perlin: Perlin) -> Self {
        Self { scale, perlin }
    }
}

impl Texture for NoiseTexture {
    fn value(&self, _: f64, _: f64, p: &Point3) -> LinearColor {
        LinearColor::white()
            * 0.5
            * (1.0 + (self.scale * p[Dim::Z] + 10.0 * self.perlin.turb(p, 7)).sin())
    }
}

impl Texture for RgbImage {
    fn value(&self, u: f64, v: f64, p: &Point3) -> LinearColor {
        let u = num::clamp(u, 0.0, 1.0);
        let v = 1.0 - num::clamp(v, 0.0, 1.0);

        let mut i = (u * self.width() as f64) as u32;
        let mut j = (v * self.height() as f64) as u32;

        if i >= self.width() {
            i = self.width() - 1;
        }
        if j >= self.height() {
            j = self.height() - 1;
        }

        let pixel = self.get_pixel(i, j);

        let scale = 1.0 / 255.0;

        LinearColor::rgb(
            pixel.0[0] as f64 * scale,
            pixel.0[1] as f64 * scale,
            pixel.0[2] as f64 * scale,
        )
    }
}
