use crate::{
    aabb::AABB,
    dims::{Dim, DimIter},
    materials::{Isotropic, Material},
    point::Point3,
    rays::Ray,
    texture::Texture,
    vec3::Vec3,
};
use rand::prelude::*;
use std::ops::Deref;
use std::rc::Rc;

pub enum NormalFace {
    FrontFace,
    BackFace,
}

pub struct HitRecord {
    pub p: Point3,
    pub normal: Vec3<f64>,
    pub material: Rc<dyn Material>,
    pub t: f64,
    pub u: f64,
    pub v: f64,
    pub facing: NormalFace,
}

impl HitRecord {
    pub fn new(
        p: Point3,
        t: f64,
        material: Rc<dyn Material>,
        outward_normal: Vec3<f64>,
        ray: &Ray,
        u: f64,
        v: f64,
    ) -> Self {
        let facing = if ray.direction.dot(&outward_normal) < 0.0 {
            NormalFace::FrontFace
        } else {
            NormalFace::BackFace
        };
        let normal = match facing {
            NormalFace::FrontFace => outward_normal,
            NormalFace::BackFace => -outward_normal,
        };
        Self {
            u,
            v,
            t,
            p,
            material,
            facing,
            normal,
        }
    }
}

pub trait Hittable {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord>;
    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB>;
}

impl<T: Deref<Target = dyn Hittable>> Hittable for Vec<T> {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        self.iter()
            .filter_map(|obj| obj.hit(r, t_min, t_max))
            .min_by(|a, b| a.t.partial_cmp(&b.t).unwrap())
    }

    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        let mut first_box = true;
        let mut resulting_box = AABB::empty();

        for hittable in self {
            match hittable.bounding_box(t0, t1) {
                Some(bb) => {
                    resulting_box = if first_box {
                        first_box = false;
                        bb
                    } else {
                        AABB::surrounding_box(bb, resulting_box)
                    }
                }
                None => return None,
            }
        }

        Some(resulting_box)
    }
}

pub struct FlipFace {
    obj: Rc<dyn Hittable>,
}

impl FlipFace {
    pub fn new(obj: Rc<dyn Hittable>) -> Self {
        Self { obj }
    }
}

impl Hittable for FlipFace {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        self.obj.hit(r, t_min, t_max).map(|mut hit| {
            hit.facing = match hit.facing {
                NormalFace::FrontFace => NormalFace::BackFace,
                NormalFace::BackFace => NormalFace::FrontFace,
            };
            hit
        })
    }
    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        self.obj.bounding_box(t0, t1)
    }
}

pub struct Translate {
    obj: Rc<dyn Hittable>,
    offset: Vec3<f64>,
}

impl Translate {
    pub fn new(obj: Rc<dyn Hittable>, offset: Vec3<f64>) -> Self {
        Self { obj, offset }
    }
}

impl Hittable for Translate {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let moved_r = Ray::new(r.origin - Point3::new(self.offset), r.direction, r.time);
        self.obj.hit(&moved_r, t_min, t_max).map(|hit| {
            HitRecord::new(
                hit.p + Point3::new(self.offset),
                hit.t,
                hit.material,
                hit.normal,
                &moved_r,
                hit.u,
                hit.v,
            )
        })
    }
    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        self.obj.bounding_box(t0, t1).map(|bb| {
            AABB::new(
                bb.min + Point3::new(self.offset),
                bb.max + Point3::new(self.offset),
            )
        })
    }
}

pub struct RotateY {
    obj: Rc<dyn Hittable>,
    cos_theta: f64,
    sin_theta: f64,
    aabb: Option<AABB>,
}

impl RotateY {
    pub fn new(obj: Rc<dyn Hittable>, angle: f64) -> Self {
        let radians = angle.to_radians();
        let sin_theta = radians.sin();
        let cos_theta = radians.cos();
        let bbox = obj.bounding_box(0.0, 1.0).unwrap_or_default();

        let mut min = Point3::xyz(f64::INFINITY, f64::INFINITY, f64::INFINITY);
        let mut max = Point3::xyz(-f64::INFINITY, -f64::INFINITY, -f64::INFINITY);

        for i in 0..2 {
            for j in 0..2 {
                for k in 0..2 {
                    let i = i as f64;
                    let j = j as f64;
                    let k = k as f64;

                    let x = i * bbox.max[Dim::X] + (1.0 - i) * bbox.min[Dim::X];
                    let y = j * bbox.max[Dim::Y] + (1.0 - j) * bbox.min[Dim::Y];
                    let z = k * bbox.max[Dim::Z] + (1.0 - k) * bbox.min[Dim::Z];

                    let newx = cos_theta * x + sin_theta * z;
                    let newz = -sin_theta * x + cos_theta * z;

                    let tester = Vec3::xyz(newx, y, newz);

                    for dim in DimIter::new() {
                        min[dim] = min[dim].min(tester[dim]);
                        max[dim] = max[dim].max(tester[dim]);
                    }
                }
            }
        }

        let aabb = AABB::new(min, max);

        Self {
            obj,
            aabb: Some(aabb),
            sin_theta,
            cos_theta,
        }
    }
}

impl Hittable for RotateY {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let mut origin = r.origin.clone();
        let mut direction = r.direction.clone();

        origin[Dim::X] = self.cos_theta * r.origin[Dim::X] - self.sin_theta * r.origin[Dim::Z];
        origin[Dim::Z] = self.sin_theta * r.origin[Dim::X] + self.cos_theta * r.origin[Dim::Z];

        direction[Dim::X] =
            self.cos_theta * r.direction[Dim::X] - self.sin_theta * r.direction[Dim::Z];
        direction[Dim::Z] =
            self.sin_theta * r.direction[Dim::X] + self.cos_theta * r.direction[Dim::Z];

        let rotated_r = Ray::new(origin, direction, r.time);

        self.obj.hit(&rotated_r, t_min, t_max).map(|hit| {
            let mut p = hit.p;
            let mut normal = hit.normal;

            p[Dim::X] = self.cos_theta * hit.p[Dim::X] + self.sin_theta * hit.p[Dim::Z];
            p[Dim::Z] = -self.sin_theta * hit.p[Dim::X] + self.cos_theta * hit.p[Dim::Z];

            normal[Dim::X] =
                self.cos_theta * hit.normal[Dim::X] + self.sin_theta * hit.normal[Dim::Z];
            normal[Dim::Z] =
                -self.sin_theta * hit.normal[Dim::X] + self.cos_theta * hit.normal[Dim::Z];

            HitRecord::new(
                hit.p,
                hit.t,
                hit.material.clone(),
                normal,
                &rotated_r,
                hit.u,
                hit.v,
            )
        })
    }

    fn bounding_box(&self, _: f64, _: f64) -> Option<AABB> {
        self.aabb.clone()
    }
}

pub struct ConstantMedium {
    boundary: Rc<dyn Hittable>,
    phase_function: Rc<dyn Material>,
    neg_inv_density: f64,
}

impl ConstantMedium {
    pub fn new(b: Rc<dyn Hittable>, d: f64, a: Rc<dyn Texture>) -> Self {
        let phase_function = Rc::new(Isotropic::new(a));
        Self {
            boundary: b,
            neg_inv_density: d,
            phase_function,
        }
    }
}

impl Hittable for ConstantMedium {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let mut rec1 = match self.boundary.hit(r, -f64::INFINITY, f64::INFINITY) {
            Some(x) => x,
            None => return None,
        };
        let mut rec2 = match self.boundary.hit(r, -f64::INFINITY + 0.0001, f64::INFINITY) {
            Some(x) => x,
            None => return None,
        };

        if rec1.t < t_min {
            rec1.t = t_min;
        }
        if rec2.t > t_max {
            rec1.t = t_max;
        }

        if rec1.t >= rec2.t {
            return None;
        }

        if rec1.t < 0.0 {
            rec1.t = 0.0;
        }

        // Too lazy to find a way to pass this in
        let mut rng = rand::thread_rng();

        let ray_length = r.direction.length();
        let distance_inside_boundary = (rec2.t - rec1.t) * ray_length;
        let hit_distance = self.neg_inv_density * rng.gen::<f64>().log2();

        if hit_distance > distance_inside_boundary {
            return None;
        }

        let t = rec1.t + hit_distance / ray_length;

        Some(HitRecord {
            t,
            p: r.at(t),
            normal: Vec3::xyz(1.0, 0.0, 0.0), // arbitruary
            facing: NormalFace::FrontFace,    // arbitrary,
            material: self.phase_function.clone(),
            u: 0.0,
            v: 0.0,
        })
    }
    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        self.boundary.bounding_box(t0, t1)
    }
}
