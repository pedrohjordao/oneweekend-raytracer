use crate::{
    aabb::AABB,
    dims::Dim,
    hit::{HitRecord, Hittable},
    rays::Ray,
};
use rand::prelude::*;
use std::{cmp::Ordering, ops::Deref, rc::Rc};

pub struct BVH<T: Deref<Target = dyn Hittable>> {
    left: Option<T>,
    right: Option<T>,
    surrounding_box: AABB,
}

impl<T: 'static + Deref<Target = dyn Hittable>> Into<Rc<dyn Hittable>> for BVH<T> {
    fn into(self) -> Rc<dyn Hittable> {
        Rc::new(self) as Rc<dyn Hittable>
    }
}

impl<T: Deref<Target = dyn Hittable> + Clone> BVH<T>
where
    Self: Into<T>,
{
    pub fn empty() -> Self {
        Self {
            left: None,
            right: None,
            surrounding_box: AABB::empty(),
        }
    }

    pub fn from_all_world(
        mut world: Vec<T>,
        time0: f64,
        time1: f64,
        rand_source: &mut ThreadRng,
    ) -> Self {
        let len = world.len();
        println!("world size: {}", len);
        Self::from_part_world(&mut world, 0, len - 1, time0, time1, rand_source)
    }

    fn from_part_world(
        world: &mut Vec<T>,
        start: usize,
        end: usize,
        time0: f64,
        time1: f64,
        rand_source: &mut ThreadRng,
    ) -> Self {
        let random_axis = match rand_source.gen_range(0, 3) {
            0 => Dim::X,
            1 => Dim::Y,
            2 => Dim::Z,
            _ => unreachable!(),
        };

        let span = end - start;

        let (left, right) = if span == 1 {
            (world.get(start).cloned(), None)
        } else if span == 2 {
            match (world.get(start), world.get(end)) {
                (Some(a), Some(b)) if box_compare(a, b, random_axis) == Ordering::Less => {
                    (Some(a.clone()), Some(b.clone()))
                }
                (Some(a), Some(b)) => (Some(b.clone()), Some(a.clone())),
                _ => unreachable!(),
            }
        } else {
            world[start..end].sort_by(|a, b| box_compare(a, b, random_axis));
            let mid = start + span / 2;
            (
                Some(Self::from_part_world(world, start, mid, time0, time1, rand_source).into()),
                Some(Self::from_part_world(world, mid, end, time0, time1, rand_source).into()),
            )
        };

        let surrounding_box = match (
            left.as_ref().and_then(|x| x.bounding_box(time0, time1)),
            right.as_ref().and_then(|x| x.bounding_box(time0, time1)),
        ) {
            (Some(l), Some(r)) => AABB::surrounding_box(l, r),
            (Some(x), None) | (None, Some(x)) => x,
            _ => {
                eprintln!("Invalid bounding box");
                AABB::empty()
            }
        };

        Self {
            left,
            right,
            surrounding_box,
        }
    }
}

impl<T: Deref<Target = dyn Hittable>> Hittable for BVH<T> {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        if self.surrounding_box.does_hit(r, t_min, t_max) {
            None
        } else {
            match self.left.as_ref().and_then(|obj| obj.hit(r, t_min, t_max)) {
                Some(rec) => self.right.as_ref().and_then(|obj| obj.hit(r, t_min, rec.t)),
                None => self.right.as_ref().and_then(|obj| obj.hit(r, t_min, t_max)),
            }
        }
    }

    fn bounding_box(&self, _: f64, _ : f64) -> Option<AABB> {
        Some(self.surrounding_box.clone())
    }
}

fn box_compare<T: Deref<Target = dyn Hittable>>(a: &T, b: &T, axis: Dim) -> Ordering {
    match (a.bounding_box(0.0, 0.0), b.bounding_box(0.0, 0.0)) {
        (None, _) | (_, None) => Ordering::Greater,
        (Some(bba), Some(bbb)) => {
            if bba.min[axis] < bbb.min[axis] {
                Ordering::Less
            } else {
                Ordering::Greater
            }
        }
    }
}
