#![feature(const_generics)]

mod aabb;
mod bvh;
mod camera;
mod const_array;
mod dims;
mod hit;
mod materials;
mod perlin;
mod point;
mod rays;
mod rgb;
mod shapes;
mod texture;
mod utils;
mod vec3;

use dims::Dim;
use point::Point3;
use rgb::{LinearColor, RgbColor, RGB};
use shapes::{rectangle::*, rectbox::RectBox, sphere::MovingSphere, sphere::Sphere};
use vec3::Vec3;

use anyhow::Result;
use bvh::BVH;
use hit::{ConstantMedium, FlipFace, Hittable, RotateY, Translate};
use image::{ImageBuffer, Rgb};
use materials::{Dielectric, DiffuseLight, Lambertian, Metal};
use rand::prelude::*;
use rays::Ray;
use std::cell::RefCell;
use std::rc::Rc;
use texture::Checkered;

const ASPECT_RATIO: f64 = 16.0 / 9.0;
const IMAGE_WIDTH: usize = 1080;
const IMAGE_HEIGHT: usize = (IMAGE_WIDTH as f64 / ASPECT_RATIO) as usize;

fn ray_color(
    ray: &Ray,
    background: &LinearColor,
    world: &dyn Hittable,
    depth: u8,
    rng: &mut ThreadRng,
) -> LinearColor {
    if depth <= 0 {
        return LinearColor::black();
    }

    match world.hit(ray, 0.0001, f64::INFINITY) {
        Some(rec) => {
            let emitted = rec.material.emmit(rec.u, rec.v, &rec.p);
            rec.material
                .scatter(ray, &rec, rng)
                .map_or(emitted, |scat| {
                    emitted
                        + scat.attenuation
                            * ray_color(&scat.scattered, background, world, depth - 1, rng)
                })
        }
        None => *background,
    }
}

fn main() -> Result<()> {
    let lookfrom = Point3::xyz(278.0, 278.0, -800.0);
    let lookat = Point3::xyz(278.0, 278.0, 0.0);
    let vup = Vec3::xyz(0.0, 1.0, 0.0);
    let apperture = 0.0;
    let dist_focus = 10.0;
    let camera = camera::Camera::new(
        lookfrom,
        lookat,
        vup,
        40.0,
        ASPECT_RATIO,
        apperture,
        dist_focus,
        0.0,
        1.0,
    );
    let samples_per_pixel = 100;

    let rng = RefCell::new(rand::thread_rng());
    let world = cornel_box(&mut rng.borrow_mut());
    let mut cnt = 0;
    let depth_limit = 50;
    let background = LinearColor::black();

    let image = ImageBuffer::from_fn(IMAGE_WIDTH as u32, IMAGE_HEIGHT as u32, |i, j| {
        cnt += 1;
        print!("\rRemaining pixels: {}", IMAGE_HEIGHT * IMAGE_WIDTH - cnt);
        let inverse_height = IMAGE_HEIGHT as u32 - j;

        let inner_color: RgbColor = (0..samples_per_pixel)
            .map(|_| {
                let mut inner_gen = rng.borrow_mut();
                (
                    (i as f64 + inner_gen.gen::<f64>()) / (IMAGE_WIDTH as f64 - 1.),
                    (inverse_height as f64 + inner_gen.gen::<f64>()) / (IMAGE_HEIGHT as f64 - 1.),
                )
            })
            .map(|(u, v)| camera.get_ray(u, v, &mut rng.borrow_mut()))
            .map(|ray| {
                ray_color(
                    &ray,
                    &background,
                    &world,
                    depth_limit,
                    &mut rng.borrow_mut(),
                )
            })
            .fold(LinearColor::black(), |acc, col| {
                acc + col / samples_per_pixel as f64
            })
            .gamma_corrected()
            .into();

        Rgb([
            inner_color[RGB::R],
            inner_color[RGB::G],
            inner_color[RGB::B],
        ])
    });

    image.save("./image.png")?;
    Ok(())
}

fn random_image(rand: &mut ThreadRng) -> impl Hittable {
    let mut world: Vec<Rc<dyn Hittable>> = Vec::new();

    let ground_material = Rc::new(Checkered::new(
        Box::new(LinearColor::rgb(0.3, 0.3, 0.1)),
        Box::new(LinearColor::rgb(0.9, 0.9, 0.9)),
    ));

    world.push(Rc::new(Sphere::new(
        Rc::new(Lambertian::new(ground_material)),
        Point3::xyz(0.0, -1000.0, 0.0),
        1000.0,
    )));

    for a in -11..11 {
        for b in -11..11 {
            let choose_mat = rand.gen::<f64>();

            let center = Point3::xyz(
                a as f64 + 0.9 + rand.gen::<f64>(),
                0.2,
                b as f64 + 0.9 * rand.gen::<f64>(),
            );

            if (center - Point3::xyz(4.0, 0.2, 0.0)).as_vec3().length() > 0.9 {
                if choose_mat < 0.8 {
                    // difuse
                    let albedo = LinearColor::random(rand) * LinearColor::random(rand);
                    let center2 =
                        center + Point3::new(Vec3::xyz(0.0, rand.gen_range(0.0, 0.5), 0.0));
                    world.push(Rc::new(MovingSphere::new(
                        Rc::new(Lambertian::new(Rc::new(albedo))),
                        center,
                        center2,
                        0.2,
                        0.0,
                        1.0,
                    )));
                } else if choose_mat < 0.95 {
                    // metal
                    let color = LinearColor::random(rand);
                    let fuzz: f64 = rand.gen_range(0.0, 0.5);
                    world.push(Rc::new(Sphere::new(
                        Rc::new(Metal::new(color, fuzz)),
                        center,
                        0.2,
                    )));
                } else {
                    // glass
                    world.push(Rc::new(Sphere::new(
                        Rc::new(Dielectric::new(1.5)),
                        center,
                        0.2,
                    )));
                }
            }
        }
    }

    world.push(Rc::new(Sphere::new(
        Rc::new(Dielectric::new(1.5)),
        Point3::xyz(0.0, 1.0, 0.0),
        1.0,
    )));
    world.push(Rc::new(Sphere::new(
        Rc::new(Lambertian::new(Rc::new(LinearColor::rgb(0.4, 0.2, 0.1)))),
        Point3::xyz(-4.0, 1.0, 0.0),
        1.0,
    )));
    world.push(Rc::new(Sphere::new(
        Rc::new(Metal::new(LinearColor::rgb(0.7, 0.6, 0.5), 0.0)),
        Point3::xyz(4.0, 1.0, 0.0),
        1.0,
    )));

    world
}

fn perlin_spheres(rng: &mut ThreadRng) -> impl Hittable {
    let perlin = perlin::Perlin::new(256, rng);
    let noise = Rc::new(texture::NoiseTexture::new(2.0, perlin));

    let earth = image::open("earthmap.jpg").unwrap().into_rgb();
    let earth_texture = Rc::new(earth);

    let light = Rc::new(DiffuseLight::new(Rc::new(LinearColor::rgb(4.0, 4.0, 4.0))));

    let world: Vec<Rc<dyn Hittable>> = vec![
        Rc::new(Sphere::new(
            Rc::new(Lambertian::new(noise.clone())),
            Point3::xyz(0.0, -1000.0, 0.0),
            1000.0,
        )),
        Rc::new(Sphere::new(
            Rc::new(Lambertian::new(earth_texture)),
            Point3::xyz(0.0, 2.0, 0.0),
            2.0,
        )),
        Rc::new(Sphere::new(light.clone(), Point3::xyz(0.0, 7.0, 0.0), 2.0)),
        Rc::new(XYRect::new(light.clone(), 3.0, 5.0, 1.0, 3.0, -2.0)),
    ];
    world
}

fn cornel_box(_: &mut ThreadRng) -> impl Hittable {
    let red = Rc::new(Lambertian::new(Rc::new(LinearColor::rgb(0.65, 0.05, 0.05))));
    let white = Rc::new(Lambertian::new(Rc::new(LinearColor::rgb(0.73, 0.73, 0.73))));
    let green = Rc::new(Lambertian::new(Rc::new(LinearColor::rgb(0.12, 0.45, 0.15))));
    let light = Rc::new(DiffuseLight::new(Rc::new(LinearColor::rgb(
        15.0, 15.0, 15.0,
    ))));

    let box1 = Rc::new(RectBox::new(
        white.clone(),
        Point3::xyz(0.0, 0.0, 0.0),
        Point3::xyz(165.0, 330.0, 165.0),
    ));
    let box1 = Rc::new(RotateY::new(box1, 15.0));
    let box1 = Rc::new(Translate::new(box1, Vec3::xyz(265.0, 0.0, 295.0)));

    let box2 = Rc::new(RectBox::new(
        white.clone(),
        Point3::xyz(0.0, 0.0, 0.0),
        Point3::xyz(165.0, 165.0, 165.0),
    ));
    let box2 = Rc::new(RotateY::new(box2, -18.0));
    let box2 = Rc::new(Translate::new(box2, Vec3::xyz(130.0, 0.0, 65.0)));

    let world: Vec<Rc<dyn Hittable>> = vec![
        Rc::new(ConstantMedium::new(
            box1,
            0.01,
            Rc::new(LinearColor::white()),
        )),
        Rc::new(ConstantMedium::new(
            box2,
            0.01,
            Rc::new(LinearColor::black()),
        )),
        Rc::new(FlipFace::new(Rc::new(YZRect::new(
            green.clone(),
            0.0,
            555.0,
            0.0,
            555.0,
            555.0,
        )))),
        Rc::new(YZRect::new(red.clone(), 0.0, 555.0, 0.0, 555.0, 0.0)),
        Rc::new(XZRect::new(
            light.clone(),
            213.0,
            343.0,
            227.0,
            332.0,
            554.0,
        )),
        Rc::new(FlipFace::new(Rc::new(XZRect::new(
            white.clone(),
            0.0,
            555.0,
            0.0,
            555.0,
            0.0,
        )))),
        Rc::new(FlipFace::new(Rc::new(XZRect::new(
            white.clone(),
            0.0,
            555.0,
            0.0,
            555.0,
            555.0,
        )))),
        Rc::new(FlipFace::new(Rc::new(XYRect::new(
            white.clone(),
            0.0,
            555.0,
            0.0,
            555.0,
            555.0,
        )))),
        Rc::new(RectBox::new(
            white.clone(),
            Point3::xyz(130.0, 0.0, 64.0),
            Point3::xyz(295.0, 165.0, 230.0),
        )),
        Rc::new(RectBox::new(
            white.clone(),
            Point3::xyz(265.0, 0.0, 295.0),
            Point3::xyz(430.0, 330.0, 460.0),
        )),
    ];
    world
}

fn final_image(rng: &mut ThreadRng) -> impl Hittable {
    let ground = Rc::new(Lambertian::new(Rc::new(LinearColor::rgb(0.48, 0.83, 0.53))));

    let mut boxes: Vec<Rc<dyn Hittable>> = Vec::new();
    for i in 0..20 {
        for j in 0..20 {
            let w = 100.0;
            let x0 = -1000.0 + j as f64 * w;
            let z0 = -1000.0 + j as f64 * w;
            let y0 = 0.0;

            let x1 = x0 + w;
            let y1 = rng.gen_range(1.0, 101.0);
            let z1 = z0 + w;

            boxes.push(Rc::new(RectBox::new(
                ground.clone(),
                Point3::xyz(x0, y0, z0),
                Point3::xyz(x1, y1, z1),
            )));
        }
    }

    let mut world: Vec<Rc<dyn Hittable>> = Vec::new();
    world.push(Rc::new(BVH::from_all_world(boxes, 0.0, 1.0, rng)));

    let light = Rc::new(DiffuseLight::new(Rc::new(LinearColor::rgb(7.0, 7.0, 7.0))));
    let center1 = Point3::xyz(400.0, 400.0, 200.0);
    let center2 = Point3::xyz(430.0, 400.0, 200.0);

    let moving_sphere_material = Rc::new(Lambertian::new(Rc::new(LinearColor::rgb(0.7, 0.3, 0.1))));

    world.push(Rc::new(MovingSphere::new(
        moving_sphere_material,
        center1,
        center2,
        0.0,
        1.0,
        50.0,
    )));
    world.push(Rc::new(Sphere::new(
        Rc::new(Dielectric::new(1.5)),
        Point3::xyz(250.0, 150.0, 45.0),
        50.0,
    )));
    world.push(Rc::new(Sphere::new(
        Rc::new(Metal::new(LinearColor::rgb(0.8, 0.8, 0.9), 10.0)),
        Point3::xyz(0.0, 150.0, 145.0),
        50.0,
    )));

    let boundary = Rc::new(Sphere::new(
        Rc::new(Dielectric::new(1.5)),
        Point3::xyz(360.0, 150.0, 145.0),
        70.0,
    ));

    world.push(boundary.clone());
    world.push(Rc::new(ConstantMedium::new(
        boundary,
        0.2,
        Rc::new(LinearColor::rgb(0.2, 0.4, 0.9)),
    )));

    let boundary2 = Rc::new(Sphere::new(
        Rc::new(Dielectric::new(1.5)),
        Point3::xyz(0.0, 0.0, 0.0),
        5000.0,
    ));
    world.push(Rc::new(ConstantMedium::new(
        boundary2,
        0.0001,
        Rc::new(LinearColor::white()),
    )));

    let earth = image::open("earthmap.jpg").unwrap().into_rgb();
    let earth_texture = Rc::new(Lambertian::new(Rc::new(earth)));
    world.push(Rc::new(Sphere::new(
        earth_texture,
        Point3::xyz(400.0, 200.0, 400.0),
        100.0,
    )));

    let pertext = Rc::new(texture::NoiseTexture::new(
        0.1,
        perlin::Perlin::new(256, rng),
    ));
    world.push(Rc::new(Sphere::new(
        Rc::new(Lambertian::new(pertext)),
        Point3::xyz(220.0, 280.0, 300.0),
        80.0,
    )));

    let mut boxes2: Vec<Rc<dyn Hittable>> = Vec::new();
    let white = Rc::new(Lambertian::new(Rc::new(LinearColor::rgb(0.73, 0.73, 0.73))));

    for j in 0..1000 {
        boxes2.push(Rc::new(Sphere::new(
            white.clone(),
            Point3::new(Vec3::random(0.0, 165.0, rng)),
            10.0,
        )));
    }

    world.push(Rc::new(Translate::new(
        Rc::new(RotateY::new(
            Rc::new(BVH::from_all_world(boxes2, 0.0, 1.0, rng)),
            15.0,
        )),
        Vec3::xyz(-100.0, 270.0, 395.0),
    )));

    world
}
