use crate::{
    dims::{Dim, DimIter},
    point::Point3,
    rays::Ray,
};
use num::Zero;
use std::mem::swap;

#[derive(Debug, Clone, Default)]
pub struct AABB {
    pub min: Point3,
    pub max: Point3,
}

impl AABB {
    pub fn new(min: Point3, max: Point3) -> Self {
        AABB { min, max }
    }

    pub fn surrounding_box(a: Self, b: Self) -> Self {
        let small = Point3::xyz(
            a.min[Dim::X].min(b.min[Dim::X]),
            a.min[Dim::Y].min(b.min[Dim::Y]),
            a.min[Dim::Z].min(b.min[Dim::Z]),
        );
        let big = Point3::xyz(
            a.max[Dim::X].max(b.max[Dim::X]),
            a.max[Dim::Y].max(b.max[Dim::Y]),
            a.max[Dim::Z].max(b.max[Dim::Z]),
        );

        Self::new(small, big)
    }

    pub fn empty() -> Self {
        Self {
            min: Point3::zero(),
            max: Point3::zero(),
        }
    }

    pub fn does_hit(&self, ray: &Ray, tmin: f64, tmax: f64) -> bool {
        for dim in DimIter::new() {
            let inv = 1.0 / ray.direction[dim];
            let mut t0 = (self.min[dim] / ray.origin[dim]) * inv;
            let mut t1 = (self.max[dim] / ray.origin[dim]) * inv;

            if inv < 0.0 {
                swap(&mut t0, &mut t1);
            }

            let min = t0.max(tmin);
            let max = t1.min(tmax);

            if max <= min {
                return false;
            }
        }
        true
    }
}
