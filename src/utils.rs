pub(crate) mod hlpr {
    use std::mem::MaybeUninit;

    pub fn array_from<F, T, const N: usize>(mut f: F) -> [T; N]
    where
        F: FnMut(usize) -> T,
    {
        // Create an explicitly uninitialized reference. The compiler knows that data inside
        // a `MaybeUninit<T>` may be invalid, and hence this is not UB:
        let mut x = MaybeUninit::<[T; N]>::uninit();
        // Set it to a valid value.
        for i in 0..N {
            unsafe {
                let ptr = x.as_mut_ptr() as *mut T;
                ptr.add(i).write(f(i));
            }
        }
        // Extract the initialized data -- this is only allowed *after* properly
        // initializing `x`!
        let x = unsafe { x.assume_init() };
        x
    }
}
