use crate::dims::Dim;
use crate::vec3::{self, Vec3};
use num::clamp;
use rand::prelude::*;
use std::iter::{IntoIterator, Iterator};
use std::ops::{
    Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Sub, SubAssign,
};

mod shaded {
    pub trait Priv: crate::vec3::Num {}
    impl Priv for u8 {}
    impl Priv for f64 {}
}

pub type RgbColor = Color<u8>;
pub type LinearColor = Color<f64>;

#[derive(Clone, Copy, Debug)]
pub struct Color<T: shaded::Priv>(Vec3<T>);

pub enum RGB {
    R,
    G,
    B,
}

impl<T: shaded::Priv> Color<T> {
    pub fn iter(&self) -> Iter<T> {
        Iter {
            iter: self.0.iter(),
        }
    }
    pub fn iter_mut(&mut self) -> IterMut<T> {
        IterMut {
            iter: self.0.iter_mut(),
        }
    }
}

impl LinearColor {
    pub fn rgb(x: f64, y: f64, z: f64) -> Self {
        Self(Vec3::xyz(
            clamp(x, 0.0, 1.0),
            clamp(y, 0.0, 1.0),
            clamp(z, 0.0, 1.0),
        ))
    }

    pub fn red() -> Self {
        LinearColor::rgb(1.0, 0.0, 0.0)
    }
    pub fn green() -> Self {
        LinearColor::rgb(0.0, 1.0, 0.0)
    }
    pub fn blue() -> Self {
        LinearColor::rgb(0.0, 0.0, 1.0)
    }

    pub fn black() -> Self {
        LinearColor::rgb(0.0, 0.0, 0.0)
    }
    pub fn white() -> Self {
        LinearColor::rgb(1.0, 1.0, 1.0)
    }

    pub fn gamma_corrected(&self) -> Self {
        Self::rgb(
            self[RGB::R].sqrt(),
            self[RGB::G].sqrt(),
            self[RGB::B].sqrt(),
        )
    }

    pub fn random(rand: &mut ThreadRng) -> Self {
        Self(Vec3::rand_unit_sphere(rand))
    }
}

impl RgbColor {
    pub fn rgb(x: u8, y: u8, z: u8) -> Self {
        Self(Vec3::xyz(x, y, z))
    }

    pub fn red() -> Self {
        RgbColor::rgb(255, 0, 0)
    }
    pub fn green() -> Self {
        RgbColor::rgb(0, 255, 0)
    }
    pub fn blue() -> Self {
        RgbColor::rgb(0, 0, 255)
    }

    pub fn black() -> Self {
        RgbColor::rgb(0, 0, 0)
    }
    pub fn white() -> Self {
        RgbColor::rgb(255, 255, 255)
    }
}

impl From<[f64; 3]> for RgbColor {
    #[inline]
    fn from(src: [f64; 3]) -> Self {
        RgbColor::rgb(
            (256.0 * clamp(src[0], 0.0, 0.9990)) as u8,
            (256.0 * clamp(src[1], 0.0, 0.9990)) as u8,
            (256.0 * clamp(src[2], 0.0, 0.9990)) as u8,
        )
    }
}

impl From<Vec3<f64>> for RgbColor {
    #[inline]
    fn from(src: Vec3<f64>) -> Self {
        RgbColor::rgb(
            (256.0 * clamp(src[Dim::X], 0.0, 0.9990)) as u8,
            (256.0 * clamp(src[Dim::Y], 0.0, 0.9990)) as u8,
            (256.0 * clamp(src[Dim::Z], 0.0, 0.9990)) as u8,
        )
    }
}

impl From<[f64; 3]> for LinearColor {
    #[inline]
    fn from(src: [f64; 3]) -> Self {
        LinearColor::rgb(
            clamp(src[0], 0.0, 1.0),
            clamp(src[1], 0.0, 1.0),
            clamp(src[2], 0.0, 1.0),
        )
    }
}

impl From<Vec3<f64>> for LinearColor {
    #[inline]
    fn from(src: Vec3<f64>) -> Self {
        LinearColor::rgb(
            clamp(src[Dim::X], 0.0, 1.0),
            clamp(src[Dim::Y], 0.0, 1.0),
            clamp(src[Dim::Z], 0.0, 1.0),
        )
    }
}

impl From<[u8; 3]> for RgbColor {
    #[inline]
    fn from(src: [u8; 3]) -> Self {
        Self(Vec3::from(src))
    }
}

impl From<&[u8]> for RgbColor {
    #[inline]
    fn from(src: &[u8]) -> Self {
        Self(Vec3::from(src))
    }
}

impl From<LinearColor> for RgbColor {
    #[inline]
    fn from(src: LinearColor) -> Self {
        Self::from(src.0)
    }
}

impl Default for RgbColor {
    #[inline]
    fn default() -> Self {
        Self::black()
    }
}

impl Default for LinearColor {
    #[inline]
    fn default() -> Self {
        Self::black()
    }
}

pub struct Iter<'a, T: shaded::Priv> {
    iter: vec3::Iter<'a, T>,
}

impl<'a, T: shaded::Priv> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
pub struct IterMut<'a, T: shaded::Priv> {
    iter: vec3::IterMut<'a, T>,
}
impl<'a, T: shaded::Priv> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
pub struct IntoIter<T: shaded::Priv> {
    counter: usize,
    inner: Color<T>,
}

impl<T: shaded::Priv> Iterator for IntoIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.counter += 1;
        let dim = match self.counter {
            1 => RGB::R,
            2 => RGB::G,
            _ => RGB::B,
        };
        if self.counter <= 3 {
            Some(self.inner[dim])
        } else {
            None
        }
    }
}

impl<T: shaded::Priv> IntoIterator for Color<T> {
    type Item = T;
    type IntoIter = IntoIter<T>;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter {
            counter: 0,
            inner: self,
        }
    }
}

impl<'a, T: shaded::Priv> IntoIterator for &'a Color<T> {
    type Item = &'a T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, T: shaded::Priv> IntoIterator for &'a mut Color<T> {
    type Item = &'a mut T;
    type IntoIter = IterMut<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<T: shaded::Priv> Index<RGB> for Color<T> {
    type Output = T;

    fn index(&self, idx: RGB) -> &Self::Output {
        match idx {
            RGB::R => &self.0[Dim::X],
            RGB::G => &self.0[Dim::Y],
            RGB::B => &self.0[Dim::Z],
        }
    }
}

impl<T: shaded::Priv> IndexMut<RGB> for Color<T> {
    fn index_mut(&mut self, idx: RGB) -> &mut Self::Output {
        match idx {
            RGB::R => &mut self.0[Dim::X],
            RGB::G => &mut self.0[Dim::Y],
            RGB::B => &mut self.0[Dim::Z],
        }
    }
}

impl<T: shaded::Priv> Add<Color<T>> for Color<T> {
    type Output = Color<T>;

    fn add(self, rhs: Color<T>) -> Self::Output {
        Color(Vec3::xyz(
            self[RGB::R] + rhs[RGB::R],
            self[RGB::G] + rhs[RGB::G],
            self[RGB::B] + rhs[RGB::B],
        ))
    }
}

impl<T: shaded::Priv> Add<T> for Color<T> {
    type Output = Color<T>;

    fn add(self, rhs: T) -> Self::Output {
        Color(Vec3::xyz(
            self[RGB::R] + rhs,
            self[RGB::G] + rhs,
            self[RGB::B] + rhs,
        ))
    }
}

impl<T: shaded::Priv> Sub<T> for Color<T> {
    type Output = Color<T>;

    fn sub(self, rhs: T) -> Self::Output {
        Color(Vec3::xyz(
            self[RGB::R] - rhs,
            self[RGB::G] - rhs,
            self[RGB::B] - rhs,
        ))
    }
}

fn multiply_lossy(a: u8, b: f64) -> u8 {
    let c = a as f64;
    let res = c * b;
    res as u8
}

impl Mul<f64> for RgbColor {
    type Output = RgbColor;

    fn mul(self, rhs: f64) -> Self::Output {
        RgbColor::rgb(
            multiply_lossy(self[RGB::R], rhs),
            multiply_lossy(self[RGB::G], rhs),
            multiply_lossy(self[RGB::B], rhs),
        )
    }
}

impl Mul<f64> for LinearColor {
    type Output = LinearColor;

    fn mul(self, rhs: f64) -> Self::Output {
        LinearColor::rgb(self[RGB::R] * rhs, self[RGB::G] * rhs, self[RGB::B] * rhs)
    }
}

impl Mul<LinearColor> for LinearColor {
    type Output = LinearColor;

    fn mul(self, rhs: LinearColor) -> Self::Output {
        LinearColor::from(self.0 * rhs.0)
    }
}

fn div_lossy(a: u8, b: f64) -> u8 {
    let c = a as f64;
    let res = c / b;
    res as u8
}

impl Div<f64> for RgbColor {
    type Output = RgbColor;

    fn div(self, rhs: f64) -> Self::Output {
        RgbColor::rgb(
            div_lossy(self[RGB::R], rhs),
            div_lossy(self[RGB::G], rhs),
            div_lossy(self[RGB::B], rhs),
        )
    }
}

impl Div<f64> for LinearColor {
    type Output = LinearColor;

    fn div(self, rhs: f64) -> Self::Output {
        LinearColor::rgb(self[RGB::R] / rhs, self[RGB::G] / rhs, self[RGB::B] / rhs)
    }
}

impl<T: shaded::Priv> Sub<Color<T>> for Color<T> {
    type Output = Color<T>;

    fn sub(self, rhs: Color<T>) -> Self::Output {
        Color(Vec3::xyz(
            self[RGB::R] - rhs[RGB::R],
            self[RGB::G] - rhs[RGB::G],
            self[RGB::B] - rhs[RGB::B],
        ))
    }
}

impl<T: shaded::Priv + AddAssign> AddAssign<Color<T>> for Color<T> {
    fn add_assign(&mut self, rhs: Color<T>) {
        self[RGB::R] += rhs[RGB::R];
        self[RGB::G] += rhs[RGB::G];
        self[RGB::B] += rhs[RGB::B];
    }
}

impl<T: shaded::Priv + AddAssign> AddAssign<T> for Color<T> {
    fn add_assign(&mut self, rhs: T) {
        self[RGB::R] += rhs;
        self[RGB::G] += rhs;
        self[RGB::B] += rhs;
    }
}

impl<T: shaded::Priv + SubAssign> SubAssign<T> for Color<T> {
    fn sub_assign(&mut self, rhs: T) {
        self[RGB::R] -= rhs;
        self[RGB::G] -= rhs;
        self[RGB::B] -= rhs;
    }
}

impl MulAssign<f64> for RgbColor {
    fn mul_assign(&mut self, rhs: f64) {
        let r = multiply_lossy(self[RGB::R], rhs);
        let g = multiply_lossy(self[RGB::G], rhs);
        let b = multiply_lossy(self[RGB::B], rhs);
        self[RGB::R] = r;
        self[RGB::G] = g;
        self[RGB::B] = b;
    }
}
impl MulAssign<f64> for LinearColor {
    fn mul_assign(&mut self, rhs: f64) {
        let r = self[RGB::R] * rhs;
        let g = self[RGB::G] * rhs;
        let b = self[RGB::B] * rhs;
        self[RGB::R] = r;
        self[RGB::G] = g;
        self[RGB::B] = b;
    }
}
impl DivAssign<f64> for RgbColor {
    fn div_assign(&mut self, rhs: f64) {
        let r = div_lossy(self[RGB::R], rhs);
        let g = div_lossy(self[RGB::G], rhs);
        let b = div_lossy(self[RGB::B], rhs);
        self[RGB::R] = r;
        self[RGB::G] = g;
        self[RGB::B] = b;
    }
}
impl DivAssign<f64> for LinearColor {
    fn div_assign(&mut self, rhs: f64) {
        let r = self[RGB::R] / rhs;
        let g = self[RGB::G] / rhs;
        let b = self[RGB::B] / rhs;
        self[RGB::R] = r;
        self[RGB::G] = g;
        self[RGB::B] = b;
    }
}
impl<T: shaded::Priv + SubAssign> SubAssign<Color<T>> for Color<T> {
    fn sub_assign(&mut self, rhs: Color<T>) {
        self[RGB::R] -= rhs[RGB::R];
        self[RGB::G] -= rhs[RGB::G];
        self[RGB::B] -= rhs[RGB::B];
    }
}

impl Mul<Vec3<f64>> for RgbColor {
    type Output = RgbColor;

    fn mul(self, rhs: Vec3<f64>) -> Self::Output {
        RgbColor::rgb(
            multiply_lossy(self[RGB::R], rhs[Dim::X]),
            multiply_lossy(self[RGB::G], rhs[Dim::Y]),
            multiply_lossy(self[RGB::B], rhs[Dim::Z]),
        )
    }
}
impl Mul<Vec3<f64>> for LinearColor {
    type Output = LinearColor;

    fn mul(self, rhs: Vec3<f64>) -> Self::Output {
        LinearColor::rgb(
            self[RGB::R] * rhs[Dim::X],
            self[RGB::G] * rhs[Dim::Y],
            self[RGB::B] * rhs[Dim::Z],
        )
    }
}

impl Div<Vec3<f64>> for RgbColor {
    type Output = RgbColor;

    fn div(self, rhs: Vec3<f64>) -> Self::Output {
        RgbColor::rgb(
            div_lossy(self[RGB::R], rhs[Dim::X]),
            div_lossy(self[RGB::G], rhs[Dim::Y]),
            div_lossy(self[RGB::B], rhs[Dim::Z]),
        )
    }
}
impl Div<Vec3<f64>> for LinearColor {
    type Output = LinearColor;

    fn div(self, rhs: Vec3<f64>) -> Self::Output {
        LinearColor::rgb(
            self[RGB::R] / rhs[Dim::X],
            self[RGB::G] / rhs[Dim::Y],
            self[RGB::B] / rhs[Dim::Z],
        )
    }
}

impl MulAssign<Vec3<f64>> for RgbColor {
    fn mul_assign(&mut self, rhs: Vec3<f64>) {
        let r = multiply_lossy(self[RGB::R], rhs[Dim::X]);
        let g = multiply_lossy(self[RGB::G], rhs[Dim::Y]);
        let b = multiply_lossy(self[RGB::B], rhs[Dim::Z]);
        self[RGB::R] = r;
        self[RGB::G] = g;
        self[RGB::B] = b;
    }
}
impl MulAssign<Vec3<f64>> for LinearColor {
    fn mul_assign(&mut self, rhs: Vec3<f64>) {
        let r = self[RGB::R] * rhs[Dim::X];
        let g = self[RGB::G] * rhs[Dim::Y];
        let b = self[RGB::B] * rhs[Dim::Z];
        self[RGB::R] = r;
        self[RGB::G] = g;
        self[RGB::B] = b;
    }
}

impl DivAssign<Vec3<f64>> for RgbColor {
    fn div_assign(&mut self, rhs: Vec3<f64>) {
        let r = div_lossy(self[RGB::R], rhs[Dim::X]);
        let g = div_lossy(self[RGB::G], rhs[Dim::Y]);
        let b = div_lossy(self[RGB::B], rhs[Dim::Z]);
        self[RGB::R] = r;
        self[RGB::G] = g;
        self[RGB::B] = b;
    }
}
impl DivAssign<Vec3<f64>> for LinearColor {
    fn div_assign(&mut self, rhs: Vec3<f64>) {
        let r = self[RGB::R] * rhs[Dim::X];
        let g = self[RGB::G] * rhs[Dim::Y];
        let b = self[RGB::B] * rhs[Dim::Z];
        self[RGB::R] = r;
        self[RGB::G] = g;
        self[RGB::B] = b;
    }
}
