use crate::dims::Dim;
use crate::vec3::{self, Vec3};
use num::{One, Zero};
use std::iter::{IntoIterator, Iterator};
use std::ops::{
    Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Sub, SubAssign,
};

#[derive(Clone, Copy, Debug)]
pub struct Point3(Vec3<f64>);

impl Point3 {
    pub fn new(v: Vec3<f64>) -> Self {
        Point3(v)
    }

    pub fn iter(&self) -> Iter {
        Iter {
            iter: self.0.iter(),
        }
    }
    pub fn iter_mut(&mut self) -> IterMut {
        IterMut {
            iter: self.0.iter_mut(),
        }
    }

    pub fn xyz(x: f64, y: f64, z: f64) -> Self {
        Self(Vec3::from([x, y, z]))
    }

    pub fn x() -> Self {
        Point3::xyz(1.0, 0.0, 0.0)
    }
    pub fn y() -> Self {
        Point3::xyz(0.0, 1.0, 0.0)
    }
    pub fn z() -> Self {
        Point3::xyz(0.0, 0.0, 1.0)
    }

    pub fn dot(&self, other: Point3) -> f64 {
        self[Dim::X] * other[Dim::X] + self[Dim::Y] * other[Dim::Y] + self[Dim::Z] * other[Dim::Z]
    }

    pub fn cross(&self, other: Point3) -> Point3 {
        Self(Vec3::from([
            self[Dim::Y] * other[Dim::Z] - self[Dim::Z] * other[Dim::Y],
            self[Dim::Z] * other[Dim::X] - self[Dim::X] * other[Dim::Z],
            self[Dim::X] * other[Dim::Y] - self[Dim::Y] * other[Dim::X],
        ]))
    }

    pub fn as_vec3(&self) -> Vec3<f64> {
        self.0
    }
}

impl From<[f64; 3]> for Point3 {
    #[inline]
    fn from(src: [f64; 3]) -> Self {
        Self(Vec3::from(src))
    }
}

impl From<&[f64]> for Point3 {
    #[inline]
    fn from(src: &[f64]) -> Self {
        Self(Vec3::from(src))
    }
}

impl Default for Point3 {
    #[inline]
    fn default() -> Self {
        Self(Vec3::default())
    }
}

impl Zero for Point3 {
    fn zero() -> Self {
        Self::xyz(0.0, 0.0, 0.0)
    }

    fn is_zero(&self) -> bool {
        self.0.into_iter().all(|x| x == 0.0)
    }
}

impl One for Point3 {
    fn one() -> Self {
        Self::xyz(1.0, 1.0, 1.0)
    }
}

impl Neg for Point3 {
    type Output = Point3;

    fn neg(self) -> Self::Output {
        Point3::xyz(-self[Dim::X], -self[Dim::Y], -self[Dim::Z])
    }
}

pub struct Iter<'a> {
    iter: vec3::Iter<'a, f64>,
}

impl<'a> Iterator for Iter<'a> {
    type Item = &'a f64;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
pub struct IterMut<'a> {
    iter: vec3::IterMut<'a, f64>,
}
impl<'a> Iterator for IterMut<'a> {
    type Item = &'a mut f64;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
pub struct IntoIter {
    counter: usize,
    inner: Point3,
}

impl Iterator for IntoIter {
    type Item = f64;

    fn next(&mut self) -> Option<Self::Item> {
        self.counter += 1;
        let dim = match self.counter {
            1 => Dim::X,
            2 => Dim::Y,
            _ => Dim::Z,
        };
        if self.counter <= 3 {
            Some(self.inner[dim])
        } else {
            None
        }
    }
}

impl IntoIterator for Point3 {
    type Item = f64;
    type IntoIter = IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter {
            counter: 0,
            inner: self,
        }
    }
}

impl<'a> IntoIterator for &'a Point3 {
    type Item = &'a f64;
    type IntoIter = Iter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a> IntoIterator for &'a mut Point3 {
    type Item = &'a mut f64;
    type IntoIter = IterMut<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl Index<Dim> for Point3 {
    type Output = f64;

    fn index(&self, idx: Dim) -> &Self::Output {
        match idx {
            Dim::X => &self.0[Dim::X],
            Dim::Y => &self.0[Dim::Y],
            Dim::Z => &self.0[Dim::Z],
        }
    }
}

impl IndexMut<Dim> for Point3 {
    fn index_mut(&mut self, idx: Dim) -> &mut Self::Output {
        match idx {
            Dim::X => &mut self.0[Dim::X],
            Dim::Y => &mut self.0[Dim::Y],
            Dim::Z => &mut self.0[Dim::Z],
        }
    }
}

impl Mul<Point3> for Point3 {
    type Output = Point3;

    fn mul(self, rhs: Point3) -> Self::Output {
        Point3::xyz(
            self[Dim::X] * rhs[Dim::X],
            self[Dim::Y] * rhs[Dim::Y],
            self[Dim::Z] * rhs[Dim::Z],
        )
    }
}

impl Add<Point3> for Point3 {
    type Output = Point3;

    fn add(self, rhs: Point3) -> Self::Output {
        Point3::xyz(
            self[Dim::X] + rhs[Dim::X],
            self[Dim::Y] + rhs[Dim::Y],
            self[Dim::Z] + rhs[Dim::Z],
        )
    }
}

impl Add<f64> for Point3 {
    type Output = Point3;

    fn add(self, rhs: f64) -> Self::Output {
        Point3::xyz(self[Dim::X] + rhs, self[Dim::Y] + rhs, self[Dim::Z] + rhs)
    }
}

impl Sub<f64> for Point3 {
    type Output = Point3;

    fn sub(self, rhs: f64) -> Self::Output {
        Point3::xyz(self[Dim::X] - rhs, self[Dim::Y] - rhs, self[Dim::Z] - rhs)
    }
}

impl Mul<f64> for Point3 {
    type Output = Point3;

    fn mul(self, rhs: f64) -> Self::Output {
        Point3::xyz(self[Dim::X] * rhs, self[Dim::Y] * rhs, self[Dim::Z] * rhs)
    }
}

impl Div<f64> for Point3 {
    type Output = Point3;

    fn div(self, rhs: f64) -> Self::Output {
        Point3::xyz(self[Dim::X] / rhs, self[Dim::Y] / rhs, self[Dim::Z] / rhs)
    }
}

impl Sub<Point3> for Point3 {
    type Output = Point3;

    fn sub(self, rhs: Point3) -> Self::Output {
        Point3::xyz(
            self[Dim::X] - rhs[Dim::X],
            self[Dim::Y] - rhs[Dim::Y],
            self[Dim::Z] - rhs[Dim::Z],
        )
    }
}

impl Div<Point3> for Point3 {
    type Output = Point3;

    fn div(self, rhs: Point3) -> Self::Output {
        Point3::xyz(
            self[Dim::X] / rhs[Dim::X],
            self[Dim::Y] / rhs[Dim::Y],
            self[Dim::Z] / rhs[Dim::Z],
        )
    }
}

impl AddAssign<Point3> for Point3 {
    fn add_assign(&mut self, rhs: Point3) {
        self[Dim::X] += rhs[Dim::X];
        self[Dim::Y] += rhs[Dim::Y];
        self[Dim::Z] += rhs[Dim::Z];
    }
}

impl AddAssign<f64> for Point3 {
    fn add_assign(&mut self, rhs: f64) {
        self[Dim::X] += rhs;
        self[Dim::Y] += rhs;
        self[Dim::Z] += rhs;
    }
}

impl SubAssign<f64> for Point3 {
    fn sub_assign(&mut self, rhs: f64) {
        self[Dim::X] -= rhs;
        self[Dim::Y] -= rhs;
        self[Dim::Z] -= rhs;
    }
}
impl MulAssign<f64> for Point3 {
    fn mul_assign(&mut self, rhs: f64) {
        self[Dim::X] *= rhs;
        self[Dim::Y] *= rhs;
        self[Dim::Z] *= rhs;
    }
}
impl DivAssign<f64> for Point3 {
    fn div_assign(&mut self, rhs: f64) {
        self[Dim::X] /= rhs;
        self[Dim::Y] /= rhs;
        self[Dim::Z] /= rhs;
    }
}
impl SubAssign<Point3> for Point3 {
    fn sub_assign(&mut self, rhs: Point3) {
        self[Dim::X] -= rhs[Dim::X];
        self[Dim::Y] -= rhs[Dim::Y];
        self[Dim::Z] -= rhs[Dim::Z];
    }
}

impl MulAssign<Point3> for Point3 {
    fn mul_assign(&mut self, rhs: Point3) {
        self[Dim::X] *= rhs[Dim::X];
        self[Dim::Y] *= rhs[Dim::Y];
        self[Dim::Z] *= rhs[Dim::Z];
    }
}

impl DivAssign<Point3> for Point3 {
    fn div_assign(&mut self, rhs: Point3) {
        self[Dim::X] /= rhs[Dim::X];
        self[Dim::Y] /= rhs[Dim::Y];
        self[Dim::Z] /= rhs[Dim::Z];
    }
}
