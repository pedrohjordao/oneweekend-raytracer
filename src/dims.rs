#[derive(Debug, Clone, Copy)]
pub enum Dim {
    X,
    Y,
    Z,
}

pub struct DimIter(Option<Dim>);

impl DimIter {
    pub fn new() -> Self {
        Self(Some(Dim::X))
    }
}

impl Iterator for DimIter {
    type Item = Dim;
    fn next(&mut self) -> Option<Self::Item> {
        match self.0 {
            Some(Dim::X) => self.0 = Some(Dim::Y),
            Some(Dim::Y) => self.0 = Some(Dim::Z),
            Some(Dim::Z) => self.0 = None,
            None => (),
        }

        self.0.clone()
    }
}
