use crate::{
    hit::{HitRecord, NormalFace},
    point::Point3,
    rays::Ray,
    rgb::LinearColor,
    texture::Texture,
    vec3::Vec3,
};
use rand::prelude::*;
use std::rc::Rc;

pub struct Scatter {
    pub attenuation: LinearColor,
    pub scattered: Ray,
}

pub trait Material {
    fn scatter(&self, ray_in: &Ray, rec: &HitRecord, rand: &mut ThreadRng) -> Option<Scatter>;
    fn emmit(&self, u: f64, v: f64, p: &Point3) -> LinearColor {
        LinearColor::black()
    }
}

pub struct Lambertian {
    albedo: Rc<dyn Texture>,
}

impl Lambertian {
    pub fn new(albedo: Rc<dyn Texture>) -> Self {
        Lambertian { albedo }
    }
}

impl Material for Lambertian {
    fn scatter(&self, r_in: &Ray, rec: &HitRecord, rand: &mut ThreadRng) -> Option<Scatter> {
        let scatter_direction = rec.normal + Vec3::<f64>::rand_unit_sphere(rand);
        let scattered = Ray::new(rec.p, scatter_direction, r_in.time);
        let attenuation = self.albedo.value(rec.u, rec.v, &rec.p);
        Some(Scatter {
            attenuation,
            scattered,
        })
    }
}

pub struct Metal {
    albedo: LinearColor,
    fuzz: f64,
}

impl Metal {
    pub fn new(albedo: LinearColor, fuzziness: f64) -> Self {
        let fuzz = if fuzziness < 1.0 { fuzziness } else { 1.0 };
        Metal { albedo, fuzz }
    }
}

impl Material for Metal {
    fn scatter(&self, ray_in: &Ray, rec: &HitRecord, rng: &mut ThreadRng) -> Option<Scatter> {
        let unit_ray_direction = ray_in.direction.unit();
        let reflected_direction = unit_ray_direction.reflect(&rec.normal);
        if reflected_direction.dot(&rec.normal) > 0.0 {
            let attenuation = self.albedo;
            let scattered = Ray::new(
                rec.p,
                reflected_direction + Vec3::<f64>::rand_unit_sphere(rng) * self.fuzz,
                0.0,
            );
            Some(Scatter {
                attenuation,
                scattered,
            })
        } else {
            None
        }
    }
}

pub struct Dielectric {
    ref_idx: f64,
}

impl Dielectric {
    pub fn new(ref_idx: f64) -> Self {
        Self { ref_idx }
    }
}

impl Material for Dielectric {
    fn scatter(&self, ray_in: &Ray, rec: &HitRecord, rand: &mut ThreadRng) -> Option<Scatter> {
        let etai_over_etat = match rec.facing {
            NormalFace::FrontFace => 1.0 / self.ref_idx,
            NormalFace::BackFace => self.ref_idx,
        };
        let unit_direction = ray_in.direction.unit();

        let cos_theta = (-unit_direction).dot(&rec.normal).min(1.0);
        let sin_theta = (1.0 - cos_theta * cos_theta).sqrt();

        let scattered = if etai_over_etat * sin_theta > 1.0
            || schlik(cos_theta, etai_over_etat) > rand.gen::<f64>()
        {
            let reflected = unit_direction.reflect(&rec.normal);
            Ray::new(rec.p, reflected, 0.0)
        } else {
            let refracted = unit_direction.refract(&rec.normal, etai_over_etat);
            Ray::new(rec.p, refracted, 0.0)
        };

        let attenuation = LinearColor::white();

        Some(Scatter {
            scattered,
            attenuation,
        })
    }
}

fn schlik(cosine: f64, ref_idx: f64) -> f64 {
    use num::pow::Pow;

    let r0 = (1.0 - ref_idx) / (1.0 + ref_idx);
    let squared = r0 * r0;
    squared + (1.0 - squared) * (1.0 - cosine).pow(5.0)
}

pub struct DiffuseLight {
    emit: Rc<dyn Texture>,
}

impl DiffuseLight {
    pub fn new(emit: Rc<dyn Texture>) -> Self {
        Self { emit }
    }
}

impl Material for DiffuseLight {
    fn scatter(&self, _: &Ray, _: &HitRecord, _: &mut ThreadRng) -> Option<Scatter> {
        None
    }

    fn emmit(&self, u: f64, v: f64, p: &Point3) -> LinearColor {
        self.emit.value(u, v, p)
    }
}

pub struct Isotropic {
    albedo: Rc<dyn Texture>,
}

impl Isotropic {
    pub fn new(albedo: Rc<dyn Texture>) -> Self { Self { albedo } }
}

impl Material for Isotropic {
    fn scatter(&self, r: &Ray, rec: &HitRecord, rng: &mut ThreadRng) -> Option<Scatter> {
        let scattered = Ray::new(rec.p, Vec3::rand_unit_sphere(rng), r.time);
        let attenuation = self.albedo.value(rec.u, rec.v, &rec.p);
        Some(Scatter { scattered, attenuation })
    }
}


