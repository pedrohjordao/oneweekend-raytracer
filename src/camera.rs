use crate::{dims::Dim, point::Point3, rays::Ray, vec3::Vec3};
use rand::prelude::*;

pub struct Camera {
    pub origin: Point3,
    pub lower_left_corner: Point3,
    pub horizontal: Vec3<f64>,
    pub vertical: Vec3<f64>,
    pub u: Vec3<f64>,
    pub v: Vec3<f64>,
    pub w: Vec3<f64>,
    pub lens_radius: f64,
    pub open_time: f64,
    pub close_time: f64,
}

impl Camera {
    pub fn new(
        lookfrom: Point3,
        lookat: Point3,
        vup: Vec3<f64>,
        vfov: f64,
        aspect_ratio: f64,
        aperture: f64,
        focus_distance: f64,
        open_time: f64,
        close_time: f64,
    ) -> Self {
        let theta = vfov.to_radians();
        let h = (theta / 2.0).tan();
        let viewport_height = 2.0 * h;
        let viewport_width = aspect_ratio * viewport_height;

        let w = (lookfrom - lookat).as_vec3().unit();
        let u = vup.cross(w).unit();
        let v = w.cross(u);

        let origin = lookfrom;
        let horizontal = u * viewport_width * focus_distance;
        let vertical = v * viewport_height * focus_distance;

        let tmp = origin.as_vec3() - horizontal / 2.0 - vertical / 2.0 - w * focus_distance;
        let lower_left_corner = Point3::new(tmp);

        let lens_radius = aperture / 2.0;

        Self {
            origin,
            lower_left_corner,
            vertical,
            horizontal,
            u,
            v,
            w,
            lens_radius,
            open_time,
            close_time,
        }
    }

    pub fn get_ray(&self, s: f64, t: f64, rand_source: &mut ThreadRng) -> Ray {
        let rd = rand_unit_disk(rand_source) * self.lens_radius;
        let offset = self.u * rd[Dim::X] + self.v * rd[Dim::Y];
        Ray::new(
            self.origin + Point3::new(offset),
            self.lower_left_corner.as_vec3() + self.horizontal * s + self.vertical * t
                - self.origin.as_vec3()
                - offset,
            rand_source.gen_range(self.open_time, self.close_time),
        )
    }
}

fn rand_unit_disk(rand: &mut ThreadRng) -> Vec3<f64> {
    let x: f64 = rand.gen_range(-1.0, 1.0);
    let squared_x = x * x;
    let y_limit = -(squared_x - 1.0);
    let y: f64 = rand.gen_range(-y_limit, y_limit);

    Vec3::<f64>::xyz(x, y, 0.0)
}
