use crate::{dims::Dim, point::Point3, vec3::Vec3};
use rand::prelude::*;

pub struct Perlin {
    point_count: usize,
    ranvec: Vec<Vec3<f64>>,
    perm_x: Vec<usize>,
    perm_y: Vec<usize>,
    perm_z: Vec<usize>,
}

impl Perlin {
    pub fn new(point_count: usize, rng: &mut ThreadRng) -> Self {
        let mut ranvec = Vec::<Vec3<f64>>::with_capacity(point_count);

        for _ in 0..point_count {
            ranvec.push(Vec3::random(-1.0, 1.0, rng));
        }

        let perm_x = perlin_generate_perm(point_count, rng);
        let perm_y = perlin_generate_perm(point_count, rng);
        let perm_z = perlin_generate_perm(point_count, rng);

        Self {
            point_count,
            ranvec,
            perm_x,
            perm_y,
            perm_z,
        }
    }

    pub fn turb(&self, p: &Point3, depth: usize) -> f64 {
        let mut acc = 0.0;
        let mut temp_p = p.clone();
        let mut weight = 1.0;

        for _ in 0..depth {
            acc += weight * self.noise(&temp_p);
            weight *= 0.5;
            temp_p *= 2.0;
        }

        acc.abs()
    }

    pub fn noise(&self, p: &Point3) -> f64 {
        let i = p[Dim::X].floor();
        let j = p[Dim::Y].floor();
        let k = p[Dim::Z].floor();

        let u = p[Dim::X] - i;
        let v = p[Dim::Y] - j;
        let w = p[Dim::Z] - k;

        let mut c: [[[Vec3<f64>; 2]; 2]; 2] = Default::default();

        for di in 0..2 {
            for dj in 0..2 {
                for dk in 0..2 {
                    let idx = self.perm_x[((i as i32 + di) & 255) as usize];
                    let idy = self.perm_y[((j as i32 + dj) & 255) as usize];
                    let idz = self.perm_z[((k as i32 + dk) & 255) as usize];

                    c[di as usize][dj as usize][dk as usize] = self.ranvec[idx ^ idy ^ idz];
                }
            }
        }

        perlin_interp(c, u, v, w)
    }
}

fn perlin_generate_perm(point_count: usize, rng: &mut ThreadRng) -> Vec<usize> {
    let mut vec = Vec::<usize>::with_capacity(point_count);

    for i in 0..point_count {
        vec.push(i);
    }

    permute(&mut vec, rng);

    vec
}

fn permute(vec: &mut Vec<usize>, rng: &mut ThreadRng) {
    for i in (1..vec.len()).rev() {
        let target = rng.gen_range(0, i);
        vec.swap(i, target);
    }
}

#[inline]
fn perlin_interp(c: [[[Vec3<f64>; 2]; 2]; 2], u: f64, v: f64, w: f64) -> f64 {
    let mut acc = 0.0;
    let u = (u * u) * (3.0 - 2.0 * u);
    let v = (v * v) * (3.0 - 2.0 * v);
    let w = (w * w) * (3.0 - 2.0 * w);

    for i in 0..2 {
        for j in 0..2 {
            for k in 0..2 {
                let new_i = i as f64;
                let new_j = j as f64;
                let new_k = k as f64;
                let weight_v = Vec3::xyz(u - new_i, v - new_j, w - new_k);
                acc += (new_i * u + (1.0 - new_i) * (1.0 - u))
                    * (new_j * v + (1.0 - new_j) * (1.0 - v))
                    * (new_k * w + (1.0 - new_k) * (1.0 - w))
                    * (c[i][j][k]).dot(&weight_v);
            }
        }
    }

    acc
}
